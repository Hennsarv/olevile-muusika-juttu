﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp16
{
    class Program
    {
        static void Main(string[] args)
        {
            MuusikabaasEntities db = new MuusikabaasEntities();
            db.Database.Log = Console.Write;
            var q = db.Songs
                .OrderByDescending(x => x.Title)
                .Select(x => new { x.Title, x.Rating });
            foreach (var x in q) Console.WriteLine(x);

            var q1 = from x in db.Songs
                     orderby x.Title descending
                     select new { x.Title, x.Rating }
                     ;
            foreach (var x in q1) Console.WriteLine(x);



            db.Database.Log = null;
            var q3 = db.Songs
                .Select(x => new { x.URL, Viimati = x.PlayLists
                                            .OrderBy(y => y.Played)
                                            .FirstOrDefault()
                                            })
                //.Select(x => new { x.URL, Viimati= x.Viimati == null ? new DateTime(1900, 1, 1) : x.Viimati.Played })
               .ToList()
                .Select( x => new { x.URL, Viimati = (x.Viimati?.Played)??(new DateTime(1900, 1, 1)) })                
              .OrderBy(x => x.Viimati) // == null ?  x.Viimati == null ? new DateTime(1900,1,1) : x.Viimati.Played : x.Viimati.Played)
                                       ////.ToList()
                ;
            foreach (var x in q3) Console.WriteLine(x);    


        }
    }
}
